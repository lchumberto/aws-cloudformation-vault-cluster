// Kubernetes Authentication
resource "vault_mount" "kubernetes_backend"{
  for_each            = var.kubernetes_backend_mounts
  depends_on          = [vault_namespace.my_namespace]
  provider            = vault.new_namespace
  type                = "kubernetes"
  path                = "kubernetes"
}

resource "vault_kubernetes_auth_backend_config" "kubernetes_config" {
  for_each            = var.kubernetes_backend_config
  depends_on          = [vault_mount.kubernetes_backend]
  provider            = vault.new_namespace
  backend             = each.value.backend
  kubernetes_host     = each.value.kubernetes_host
  kubernetes_ca_cert  = each.value.kubernetes_ca_cert
  token_reviewer_jwt  = each.value.token_reviewer_jwt
  issuer              = each.value.issuer
}

resource "vault_kubernetes_auth_backend_role" "kubernetes_role" {
  for_each                         = var.kubernetes_backend_roles
  depends_on                       = [vault_mount.kubernetes_backend, vault_kubernetes_auth_backend_config.kubernetes_config]
  provider                         = vault.new_namespace
  backend                          = each.value.backend
  role_name                        = each.value.role_name
  bound_service_account_names      = each.value.bound_service_account_names
  bound_service_account_namespaces = each.value.bound_service_account_namespaces
  token_ttl                        = each.value.token_ttl
  token_policies                   = each.value.token_policies
  audience                         = each.value.audience
}
